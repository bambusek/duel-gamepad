# Duel Gamepad

DIY gamepad designed for playing [Duel 6 Reloaded](https://github.com/odanek/duel6r) game (but you can really use it for any arcade game). Box is made from laser cut aspen plywood, hardware consists of USB encoder, buttons, joystick and wires.

## Hardware

Hardware consists of USB encoder, buttons, joystick and bunch of wires to connect them together and one USB cable for connecting the gamepad to a computer. This complete kit can be bought on Ebay (search for *Zero Delay Arcade Game DIY Kit*) for around 20$. The one used on pictures was bough from [this Ebay seller](https://www.ebay.com/itm/Zero-Delay-Arcade-Game-DIY-Kits-Parts-10-Color-Buttons-Joystick-USB-Encoder-/202288138240?hash=item2f19500800).

![Hardware](https://gitlab.com/bambusek/duel-gamepad/raw/13f0e2c7fb43d6be2f9ea01b9b83c56391bc5898/assets/hardware.jpg)

## Box

The box is made from aspen 6mm thick plywood and was laser cut on Trotec Speedy 300. Source files for cutting the box can be found in this repository. There are two available versions - first one containing Duel logo, button labels and QR code with link to this repository (*duel_gamepad_engravements*) and the second one without them (*duel_gamepad*). That's so you don't have to delete them by yourself if you'd wish not to have the Duel customization on your gamepad. The source files are available in two formats `.pdf` and `.cdr`. The `.cdr` file format belongs to CorelDraw and it was created in version 2018.

The red line in source files is meant to be cut, the black parts to be engraved. The settings for the Trotec Speedy 300 laser cutter are summarized below.

Color | | Mode | Power | Speed
----- | --- | ---- | ----- | -----
Red | ![#FF0000](https://placehold.it/25/FF0000/FF0000) | Cut | 100 | 0.7
Black | ![#000000](https://placehold.it/25/000000/000000) | Engrave| 80| 60

You need a plywood at least 46cm wide and 36cm height to fit the box parts on it.

The top part can be attached to the rest (which should be glued together) by screws. The box is designed for M3 screws (3mm wide) that are 16mm long and M3 nuts (5.5mm is the smallest width). You will need 6 screws and nuts to attach the box together and 6 nuts and screws to attach the joystick to the box.

The box was cut in [Brno FabLab](https://www.fablabbrno.cz/).

![Hardware](https://gitlab.com/bambusek/duel-gamepad/raw/13f0e2c7fb43d6be2f9ea01b9b83c56391bc5898/assets/fablab_logo.png)

## Wiring the parts

Connecting the buttons and joystick to the USB encoder is easy, see the image below for details.

![Hardware](https://gitlab.com/bambusek/duel-gamepad/raw/13f0e2c7fb43d6be2f9ea01b9b83c56391bc5898/assets/usb_encoder.jpg)
